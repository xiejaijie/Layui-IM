package com.service;

import com.bean.Mine;
import com.mapper.FriendsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FriendsService {
    @Autowired
    FriendsMapper friendsMapper;
    //查询用户的好友列表
    public List<Mine> LookUserFriend(String userid){
        return friendsMapper.LookUserFriend(userid);
    }
    public Mine LookUserMine(String userid){
        return friendsMapper.LookUserMine(userid);
    }
}
