package com.controller;

import com.bean.Mine;
import com.service.FriendsService;
import com.service.GroupsService;
import com.util.exception.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class GroupsCtrl {
    @Autowired
    GroupsService groupsService;
    @Autowired
    FriendsService friendsService;
    /**
     * 查询群组的成员
     * **/
    @GetMapping(value = "/group/member")
    @ResponseBody
    public R groupmember(String id){
        List<String> list1 = groupsService.LookGroupUserid(id);
        List<Mine> list=new ArrayList<>();
        for (String s:list1) {
            list.add(friendsService.LookUserMine(s));
        }
        return R.ok().data("list",list).message("群成员");
    }
    /**
     * TODO 查询群组聊天记录
     * */
    @GetMapping("/group/chatlog/{gid}")
    @ResponseBody
    public List<Mine> chatlog(@PathVariable("gid")String gid) throws InterruptedException {
        Thread.sleep(1000);//模拟消息查询缓慢，让前台展示loading样式
        List<Mine> mines = groupsService.LookGroupMsg(gid);
        return mines;
    }
}
