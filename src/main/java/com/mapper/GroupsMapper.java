package com.mapper;

import com.bean.Groups;
import com.bean.Groupsmsg;
import com.bean.Mine;

import java.util.List;

public interface GroupsMapper {
    //查询用户的群
    Groups LookUserGroups(String userid);
    //查询群组的成员
    List<String> LookGroupUserid(String id);
    //插入群聊消息
    void InsertGroupMsg(Groupsmsg groupsmsg);
    //查询群组的聊天记录
    List<Mine> LookGroupMsg(String gid);
}
