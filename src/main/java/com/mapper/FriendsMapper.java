package com.mapper;


import com.bean.Mine;

import java.util.List;

public interface FriendsMapper {
    //查询用户的好友列表
    List<Mine> LookUserFriend(String userid);
    //查询用户的信息
    Mine LookUserMine(String userid);
}